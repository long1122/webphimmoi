

function movieSelected(id){
    sessionStorage.setItem('movieId',id);
    window.location='movie.html';
    return false;
}

function getMovie(){
    let movieId=sessionStorage.getItem('movieId');
    axios.get('http://www.omdbapi.com/?apikey=7640cd71&i=' + movieId) 
    .then((response)=>{
        console.log(response);
        let movie=response.data;

        let output=`
           <div class="row">
               <div class="col-md-4">
                   <img src="${movie.Poster}" class="thumbnail">
               </div>
               <div class="col-md-8">
                   <h2>${movie.Title}</h2>
                   <ul class="list-group">
                       <li class="list-group-item"><strong>Thể loại: </strong>${movie.Genre}</li>
                       <li class="list-group-item"><strong>Năm phát hành: </strong>${movie.Released}</li>
                       <li class="list-group-item"><strong>Đánh giá: </strong>${movie.Rated}</li>
                       <li class="list-group-item"><strong>Top IMDB: </strong>${movie.imdbRating}</li>
                       <li class="list-group-item"><strong>Đạo diễn: </strong>${movie.Director}</li>
                       <li class="list-group-item"><strong>Tác giả: </strong>${movie.Writer}</li>
                       <li class="list-group-item"><strong>Diễn viên chính: </strong>${movie.Actors}</li>
                   </ul>
               </div>
           </div>
           <div class="row">
                <div class="well">
                    <h3>Tóm tắc: </h3>
                    ${movie.Plot}
                    <hr>
                    <a href="http://imdb.com/title/${movie.imdbID}" target="blank" class="btn btn-primary">View IMDB</a>
                    <a href="./index.html" class="btn btn-default">Quay lại</a>
                </div>
           </div>
        `;
        $('#movie').html(output);
    })
    .catch((err)=>{
        console.log(err);
    });
}

async function loadPage(searchText,page){
    const response=await fetch(`http://www.omdbapi.com/?i=tt3896198&apikey=7640cd71&s=${searchText}&page=${page}`);
    const users=await response.json();
    console.log(users);
    $('#movies').empty();
        let output='';
        let movies=users.Search;
        $.each(movies, (index, movie) =>{
            output += `
            <div class="col-md-3">
                <div class="well text-center">
                     <img src="${movie.Poster}">
                     <h5>${movie.Title}</h5>
                     <a onclick="movieSelected('${movie.imdbID}')" class="btn btn-primary" href="#"> Chi tiết phim</a>
                </div>
            </div>
            `;
        });
        $('#movies').html(output);

}
async function run(searchText,page){
    const response=await fetch(`http://www.omdbapi.com/?i=tt3896198&apikey=7640cd71&s=${searchText}&page=${2}`);
    const datas=await response.json();
    console.log(datas);
    loadPage(searchText,1);
      
}

$(document).ready(()=> {
    let searchText1;
    let page=1;
    $('#searchForm').on('submit',(e) => {
        let searchText=$('#searchText').val();
        searchText1=searchText;
        run(searchText,page);
        e.preventDefault();
    });
    $("#item1 a").click(function () {
        page = page - 1;
        alert(page);
        if (page >= 0) {
            loadPage(searchText1,page);
        }
    });
    $("#item2 a").click(function () {
        page = page + 1;
        alert(page);
        if (page <= 100) {
            loadPage(searchText1,page);
        }
    });
});




